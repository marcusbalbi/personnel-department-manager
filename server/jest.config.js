module.exports = {
  rootDir: './',
  globalSetup: './config/test/setup.js',
  globalTeardown: './config/test/teardown.js',
  // setupTestFrameworkScriptFile: './test/setuptest.js',
  coverageDirectory: './test/coverage',
  // transform: {
  //   '^.+\\.jsx?$': 'babel-jest'
  // },
  // collectCoverage: true
  'moduleNameMapper': {
    '^@root(.*)$': '<rootDir>/$1',
    '^@app(.*)$': '<rootDir>/src/app$1',
    '^@common(.*)$': '<rootDir>/src/common$1'
  }
}
